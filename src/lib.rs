use reqwest::Client;
use serde_derive::Deserialize;
use serde_json as json;

const MOJANG_SESSION_SERVER: &str = "https://sessionserver.mojang.com/";
const SESSION_ENDPOINT: &str = "session/minecraft/";

#[derive(Deserialize, Debug)]
pub struct AuthResponse {
    pub id: String,
    pub name: String,
    pub properties: json::Value
}

pub struct MojangClient {
    client: Client
}

impl MojangClient {
    pub fn new() -> MojangClient {
        MojangClient {
            client: Client::new()
        }
    }

    pub async fn auth_with_yggdrasil(&self, username: &str, server_id: &str) -> Result<AuthResponse, reqwest::Error> {
        self.client.get(
            &format!(
                "{}{}hasJoined?username={}&serverId={}",
                MOJANG_SESSION_SERVER,
                SESSION_ENDPOINT,
                username,
                server_id))
            .send().await?
            .json().await
    }
}
