# mojang-rs

Rust wrapper for the [Mojang API](http://wiki.vg/Mojang_API) and [Mojang Authentication API](http://wiki.vg/Authentication).
